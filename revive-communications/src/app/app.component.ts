import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;

  public appPages = [
    {
        title: 'Financial Contribution',
        icon: 'card',
        url: '#',
        // title: 'Profile',
        // icon: 'person-circle',
        // children: [
        //   {
        //     title: 'Update Profile',
        //     icon: 'pencil',
        //   },
        //   {
        //     title: 'Financial Contribution',
        //     icon: 'credit',
        //   },
        //   {
        //     title: 'Logout',
        //     icon: 'log-out',
        //   },
  
        // ],
      },
      {
        title: 'Manage Members',
        icon: 'people',
        children: [
          {
            title: 'View Members',
            icon: 'eye',
          },
          {
            title: 'Activate Members',
            icon: 'person-add',
          },
          {
            title: 'Deactivate Members',
            icon: 'person-remove',
          },
          {
            title: 'Transfer Members',
            icon: 'swap-horizontal',
          },
  
        ],
  
      },
      {
        title: 'Message Members',
        icon: 'mail',
        children: [
          {
            title: 'Send Invitation',
            icon: 'caret-forward',
            url: 'send-invitation'

  
          },
          {
            title: 'Post Announcement',
            icon: 'caret-forward',
            url: 'post-announcement'

          },
          {
            title: 'Remove Announcement',
            icon: 'caret-forward',
            url: 'remove-announcement'
          },
        ],
  
      },
       {
        title: 'Groups',
        icon: 'people-circle',
      },
       {
        title: 'Homecell notes',
        icon: 'document-attach',
      },
       {
        title: 'Kids Church',
        icon: 'home',
  
      },
  
      {
        title: 'Follow-up',
        icon: 'call',
        children: [
          {
            title: 'Salvation',
            icon: 'caret-forward',
  
          },
          {
            title: 'Requests to Serve',
            icon: 'caret-forward',
          },
          {
            title: 'NMO',
            icon: 'caret-forward',
          },
          {
            title: 'Overseers',
            icon: 'caret-forward',
          },
          {
            title: 'Leaders',
            icon: 'caret-forward',
          },
          {
            title: 'Members',
            icon: 'caret-forward',
          },
          {
            title: 'Discipleship',
            icon: 'caret-forward',
            url: 'discipleship-followup'
          },
        ],
  
      },
      {
        title: 'Set Goals',
        icon: 'trophy',
        url: '#',
 
      },
      {
        title: 'Feedback',
        icon: 'stats-chart',
        children: [
          {
            title: 'Homecell Attendance',
            icon: 'caret-forward',
  
          },
          {
            title: 'Church Attendance',
            icon: 'caret-forward',
          },
          {
            title: 'Discipleship Attendance',
            icon: 'caret-forward',
          },
          {
            title: 'Structure Growth',
            icon: 'caret-forward',
            url:'structure-growth-feedback'
          },
          {
            title: 'New Member Orientation',
            icon: 'caret-forward',
            url: 'nmofeedback'
          },
          {
            title: 'Zone Growth',
            icon: 'caret-forward',
          },
          {
            title: 'Zone HC Attendance',
            icon: 'caret-forward',
          },
          {
            title: 'Zone Church Attendance',
            icon: 'caret-forward',
          },
  
        ],
  
      },
      {
        title: 'Reports',
        icon: 'trending-up',
  
        children: [
          {
            title: 'Zone Growth',
            icon: 'caret-forward',
  
          },
          {
            title: 'Overview Of Structure',
            icon: 'caret-forward',
            url: 'overview-structure'
          },
          {
            title: 'Discipleship Progress',
            icon: 'caret-forward',
          },
        ],
  
      },
      // {
      //   title: 'Admin',
      //   icon: 'settings',
  
      //   children: [
      //     {
      //       title: 'User Roles',
      //       icon: 'caret-forward',
  
      //     },
      //     {
      //       title: 'Group Types',
      //       icon: 'caret-forward',
  
      //     },
      //     {
      //       title: 'Discipleships',
      //       icon: 'caret-forward',
      //       url: 'view-discipleship'
      //     },
      //     {
      //       title: 'Structure Positions',
      //       icon: 'caret-forward',
      //     },
      //     {
      //       title: 'Individual Positions',
      //       icon: 'caret-forward',
      //       url: 'view-ind-pos'
      //     },
      //     {
      //       title: 'Individual Position Goals',
      //       icon: 'caret-forward',
      //     },
      //   ],
  
      // },
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
