import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-kids-church-check-in',
  templateUrl: './kids-church-check-in.page.html',
  styleUrls: ['./kids-church-check-in.page.scss'],
})
export class KidsChurchCheckInPage implements OnInit {

  constructor(public loadingController: LoadingController, public alertController: AlertController) {}

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Checked-in successfully',
      message: 'Jana Anderson & Josh Anderson was successfully checked-in @ CRC Main Kids Church 8:25 Am 2020-05-06.    Start time: 8:30      End time: 9:30',
      buttons: ['OK']
    });

    await alert.present();
  }
  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait for confirmation from volunteer...',
      duration: 3000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

    this.presentAlert();
  }

  ngOnInit() {
  }

}
