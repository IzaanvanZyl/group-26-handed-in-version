import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-announcement',
  templateUrl: './post-announcement.page.html',
  styleUrls: ['./post-announcement.page.scss'],
})
export class PostAnnouncementPage implements OnInit {

  Persons = [
    {key: '1', text: "Person 1"},
    {key: '2', text: "Person 2"},
    {key: '3', text: "Person 3"}
    ];
      
  constructor() { }

  ngOnInit() {
  }

}
