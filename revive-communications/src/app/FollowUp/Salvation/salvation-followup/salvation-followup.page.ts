import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-salvation-followup',
  templateUrl: './salvation-followup.page.html',
  styleUrls: ['./salvation-followup.page.scss'],
})
export class SalvationFollowupPage implements OnInit {
  ProgressCheck: boolean;
  constructor() { }

  ngOnInit() {
    this.ProgressCheck = false;
  }

  Check(){
    if(this.ProgressCheck == false)
    {
      this.ProgressCheck = true;
    }
    else{
      this.ProgressCheck = false;
    }
    
  }
}
