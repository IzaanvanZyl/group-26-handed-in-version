import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-members-wanting-to-serve-follow-up',
  templateUrl: './members-wanting-to-serve-follow-up.page.html',
  styleUrls: ['./members-wanting-to-serve-follow-up.page.scss'],
})
export class MembersWantingToServeFollowUpPage implements OnInit {
  ProgressCheck: boolean;
  groups: boolean;
  constructor() { }

  ngOnInit() {
    this.ProgressCheck = false;
    this.ProgressCheck = false;
  }

  Check(){
    if(this.ProgressCheck == false)
    {
      this.ProgressCheck = true;
    }
    else{
      this.ProgressCheck = false;
    }
    
    
  }
  CheckGroups(){
      
    if(this.groups == false)
    {
      this.groups = true;
    }
    else{
      this.groups = false;
    }
  }}
